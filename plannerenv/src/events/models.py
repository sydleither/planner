from django.db import models
from django.urls import reverse

# Create your models here.

class Event(models.Model):
	title = models.CharField(max_length=20)
	type = models.CharField(
		max_length = 8,
		choices = (
			('Event', 'Event'),
			('Work', 'Work'),
			('Reminder', 'Reminder'),
			('Homework', 'Homework'),
		)
	)
	day = models.DateField(u'Day of Event')
	start_time = models.TimeField(u'Start Time', null=True)
	end_time = models.TimeField(u'End Time', null=True)
	
	def get_absolute_url(self):
		url = reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name), args=[self.id])
		return u'<a href="%s">%s</a>' % (url, str(self.title))